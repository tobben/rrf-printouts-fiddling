##  RRF Printouts Fiddling

Sometimes when I program changes to ReprapFirmware printouts, I want to test them without having to go via the target hardware. So I copied some files over here that can
be compiled and tested in isolation.
