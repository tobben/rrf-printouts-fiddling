//#include "/home/torbjorn/eclipse-workspace/RRFLibraries/src/General/Portability.h"
#include "StringRef.h"
#include "String.h"
#include <iostream>


enum class HangprinterAnchorMode {
	None, // All is reacheable in None anchor mode as printing volume
	LastOnTop, // (Default) Rsults in a pyramid plus a prism below if the lower anchors are above the printing bed
	AllOnTop, // Result in a prism (speeds get limited, specially going down in Z)
};


auto main() -> int {

	String<256> reply_;
	static constexpr size_t HANGPRINTER_MAX_ANCHORS = 5;
	size_t numAnchors = 4;
	const StringRef& reply = reply_.GetRef();
	HangprinterAnchorMode anchorMode = HangprinterAnchorMode::LastOnTop;
	float spoolBuildupFactor = 0.0F;
	float spoolRadii[HANGPRINTER_MAX_ANCHORS] = { 0.0F };
	uint32_t mechanicalAdvantage[HANGPRINTER_MAX_ANCHORS] = { 0 };
	uint32_t linesPerSpool[HANGPRINTER_MAX_ANCHORS] = { 0 };
	uint32_t motorGearTeeth[HANGPRINTER_MAX_ANCHORS] = { 0 };
	uint32_t spoolGearTeeth[HANGPRINTER_MAX_ANCHORS] = { 0 };
	uint32_t fullStepsPerMotorRev[HANGPRINTER_MAX_ANCHORS] = { 0 };
	float moverWeight_kg = 0.0F;
	float springKPerUnitLength = 0.0F;
	float minPlannedForce_Newton[HANGPRINTER_MAX_ANCHORS] = { 0.0F };
	float maxPlannedForce_Newton[HANGPRINTER_MAX_ANCHORS] = { 0.0F };
	float guyWireLengths[HANGPRINTER_MAX_ANCHORS] = { 0.0F };
	float targetForce_Newton = 0.0F;
	float torqueConstants[HANGPRINTER_MAX_ANCHORS] = { 0.0F };



	reply.printf("M666 A%u Q%.4f\n", (unsigned)anchorMode, (double)spoolBuildupFactor);

	reply.lcatf("R%.2f", (double)spoolRadii[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%.2f", (double)spoolRadii[i]);
	}

	reply.lcatf("U%d", (int)mechanicalAdvantage[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%d", (int)mechanicalAdvantage[i]);
	}

	reply.lcatf("O%d", (int)linesPerSpool[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%d", (int)linesPerSpool[i]);
	}

	reply.lcatf("L%d", (int)motorGearTeeth[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%d", (int)motorGearTeeth[i]);
	}

	reply.lcatf("H%d", (int)spoolGearTeeth[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%d", (int)spoolGearTeeth[i]);
	}

	reply.lcatf("J%d", (int)fullStepsPerMotorRev[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%d", (int)fullStepsPerMotorRev[i]);
	}
	reply.lcatf("W%.2f\n", (double)moverWeight_kg);
	reply.lcatf("S%.2f\n", (double)springKPerUnitLength);

	reply.lcatf("I%.1f", (double)minPlannedForce_Newton[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%.1f", (double)minPlannedForce_Newton[i]);
	}

	reply.lcatf("X%.1f", (double)maxPlannedForce_Newton[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%.1f", (double)maxPlannedForce_Newton[i]);
	}

	reply.lcatf("Y%.1f", (double)guyWireLengths[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%.1f", (double)guyWireLengths[i]);
	}
	reply.lcatf("T%.1f\n", (double)targetForce_Newton);

	reply.lcatf("C%.4f", (double)torqueConstants[0]);
	for (size_t i = 1; i < numAnchors; ++i)
	{
		reply.catf(":%.4f", (double)torqueConstants[i]);
	}
	printf("%s\n", reply.c_str());
}
